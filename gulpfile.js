let gulp = require('gulp');
let sass = require('gulp-sass');
let sync = require('browser-sync');

gulp.task('sass', function () {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass({}))
        .pipe(gulp.dest('./dist/css'))
        .pipe(sync.reload({stream: true}))

});

gulp.task('watch', function () {
    gulp.watch('./src/scss/**/*.scss', gulp.series('sass'));
    gulp.watch('./dist/*.html', gulp.parallel('sass'));

});

gulp.task('sync', function () {
    sync.init({
        server: {
            baseDir: './dist'
        }
    })
});

gulp.task('html', function () {
    return gulp.src('./dist/*.html')
        .pipe(sync.reload({stream: true}));
});

gulp.task('default',  gulp.parallel('sync', 'watch'));